

void parseCmd(char *cmd){
  char c = cmd[0];
  ++cmd;
  if ( !c )
    return;

  if ( !strchr("henlirsg", c) ){
    Serial.println(F("unknown cmd, type 'h' for help"));
    return;
  }

  if ( c == 'h' ){
    Serial.print(F(
      "h .  .  .  .  .  .  .  .  this help\r\n"
      "e .  .  .  .  .  .  .  .  toggle echo\r\n"
      "l .  .  .  .  .  .  .  .  list entries\r\n"
      "i <key> <val> .  .  .  .  insert entry\r\n"
      "r <key> .  .  .  .  .  .  remove entry\r\n"
      "g <key> .  .  .  .  .  .  get entry\r\n"
      "s <key> <newKey> <newVal> set entry\r\n"
    ));
    return;
  }

  if ( c == 'e' ){
     remoteEcho = !remoteEcho;
     Serial.print(F("remote echo is now "));
     Serial.println(remoteEcho? "on": "off");
     return;
  }

  if ( c == 'l' ){
    echo(F("count: ") _ assoc.count());
    assoc.walk([](Entry e){
      echo('[' _ e.key _ ',' _ e.val _ ']');
      return false;
    });
    return;
  }

  static char *strKey;
  strKey = strtok_r(cmd, " ", &cmd);
  if ( !strKey || strlen(strKey) > 6 ){
    Serial.println(F("invalid key name"));
    return;
  }

  Entry *entry = assoc.walk([](Entry e){
    if ( !strcmp(e.key, strKey) )
      return true;
    return false;
  }, c == 'r');

  if ( c == 'i' ){
    if ( entry ){
      Serial.println(F("duplicated key"));
      return;
    }
    entry = assoc.insert();
  }
  else if ( !entry ){
    Serial.println(F("entry not found"));
    return;
  }

  if ( c == 'r' )  // already removed above
    return;

  if ( c == 'g' ){
    Serial.println(entry->val);
    return;
  }
  
  if ( c == 's' ){
    strKey = strtok_r(cmd, " ", &cmd);
    if ( !strKey || strlen(strKey) > 6 ){
      Serial.println(F("invalid new key name"));
      return;
    }
  }

  int intVal = strToInt(&cmd);
  if ( errorFlag ){
    Serial.println(F("invalid int value"));
    return;
  }

  // at this point remaining commands are just "insert" or "set"
  strcpy(entry->key, strKey);  // doesn't search for duplicated key
  entry->val = intVal;
}
