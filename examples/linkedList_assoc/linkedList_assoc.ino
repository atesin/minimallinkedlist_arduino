
// emulate print concatenation with echo("ratio: " _ val _ '%')
#define echo(args) do{if(Serial){Serial.print(args);Serial.println();}}while(0)
#define _ );Serial.print(


#include <MinimalLinkedList.h>
  
struct Entry{
  char key[7] = "";
  int val = 0;
};

MinimalLinkedList<Entry> assoc;

bool remoteEcho = true;
bool errorFlag;


void setup()
{
  Serial.begin(115200);
  while ( !Serial )
    delay(0);
  delay(500);
  Serial.println();

  Serial.print(F("interactive linked list test, with associative array of [str,int]\r\ntype 'h' for help\r\n> "));
}


void loop(){
  if ( Serial.available() ){
    // real max length = 99, 100 = overflow, 101 = nul terminator
    char rxBuf[101];
    userInput(rxBuf, 100);
    parseCmd(rxBuf);
    Serial.print("> ");
  }
}
