
void parseCmd(char *cmd){
  char c = cmd[0];
  ++cmd;
  if ( !c )
    return;

  if ( !strchr("helirfm", c) ){
    Serial.println(F("unknown cmd, type 'h' for help"));
    return;
  }

  if ( c == 'h' ){
    Serial.print(F(
      "h  .  .  .  .  .  this help\r\n"
      "e  .  .  .  .  .  toggle echo\r\n"
      "l  .  .  .  .  .  list numbers\r\n"
      "i <val>  .  .  .  insert number\r\n"
      "r <val>  .  .  .  remove number\r\n"
      "f <val>  .  .  .  find number (no output = found)\r\n"
      "m <val> <newVal>  modify number\r\n"
    ));
    return;
  }

  if ( c == 'e' ){
     remoteEcho = !remoteEcho;
     Serial.print(F("remote echo is now "));
     Serial.println(remoteEcho? "on": "off");
     return;
  }

  if ( c == 'l' ){
    echo(F("count: ") _ ints.count());
    ints.walk([](int i[1]){
      Serial.println(i[0]);
      return false;
    });
    return;
  }

  static int intVal;
  intVal = strToInt(&cmd);
  if ( errorFlag ){
    Serial.println(F("invalid int value"));
    return;
  }

  int *i = *ints.walk([](int *i){
    if ( i[0] == intVal )
      return true;
    return false;
  }, c == 'r');

  if ( c == 'i' ){
    if ( i ){
      Serial.println(F("duplicated element"));
      return;
    }
    i = *ints.insert();
    i[0] = intVal;
  }
  else if ( !i ){
    Serial.println(F("int not found"));
    return;
  }

  if ( c == 'r' )  // already removed above
    return;

  if ( c == 'f' )  // already found above
    return;

  if ( c == 'm' ){
    intVal = strToInt(&cmd);
    if ( errorFlag ){
      Serial.println(F("invalid new int value"));
      return;
    }
  }
  
  // at this point remaining commands are just "insert" or "modify"
  i[0] = intVal;
}
