
// fill a string buffer with user input when Serial.available()
char* userInput(char *rxBuf, size_t maxLen){
  rxBuf[Serial.readBytesUntil('\n', rxBuf, maxLen)] = '\0';
  if ( strlen(rxBuf) >= maxLen ){
    // "real" flush
    while ( size_t avail = Serial.available() ){
      while ( avail-- )
        Serial.read();
      delay(2);
    }
    Serial.println(F("cmd too long"));
    return;
  }
  if ( char *cr = strrchr(rxBuf, '\r') )
    cr[0] = '\0';
  if ( remoteEcho )
    Serial.println(rxBuf);
  return rxBuf;
}


// tries to read the integer value from a number-looking string
// sets global errorFlag accordingly (true on error)
// on success it also advances the string pointer after the number-looking part
int strToInt(char **strVal){
  char *origStr = *strVal;
  long longVal = strtol(origStr, strVal, 10);
  int intVal = (int) longVal;
  errorFlag = *strVal == origStr || intVal != longVal;
  return intVal;
}
