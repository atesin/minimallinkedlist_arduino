/** 
Minimal Linked List arduino class library v1.0.0
released under gpl v3 by atesin@gitlab in nov 2022

https://gitlab.com/atesin/minimallinkedlist_arduino

i know there are many similar libs, but i wrote this for fun, for learn and for
memory efficiency, since it has only 3 functions

it comes without any warranty, you are the responsible to do whatever you want with it
and the consequences, however well intentioned contributions are welcome, i encourage you
to read and understand the code, and to read (and enrich if you can) the online documentation
*/


template <typename T> class MinimalLinkedList{
  
  private:
  
    // basic linked list node
    // can allocate and store any data type that can point and modify its address directly (array, struct, object, etc)
    // (raw primitives and derives doesn't work because are assigned by copy and the link to allocated memory is lost)
    struct Node{
      T storedData;
      Node *nextNode;  // this makes what linked lists are
    };
    
    // pointer to most recent linked list Node where to start iterations
    Node *headNode = NULL;
    
    // variable to track nodes count, not really needed but useful
    size_t nodesCount = 0;
    
    // signature prototype for callback functions to be applied on list nodes with walk()
    // returns true to stop iteration and return (or extract) matched element
    // notice the argument is the element itself not a pointer to
    typedef bool (*testNode)(T element);
    
  public:

    // allocates and inserts a new node to the list with a default element
    // returns a pointer to the new element to do whatever you want with it
    T* insert(){
      Node *newNode = new Node;
      newNode->nextNode = headNode;
      headNode = newNode;
      ++nodesCount;
      return &newNode->storedData;
    }

    // returns current elements count on this list
    // i choose "count" because "size" or "length" is often used to return single bytes in C
    size_t count(){
      return nodesCount;
    }

    // applies a callback function to all list nodes, it must be "bool myFunc(MyType someVar)" as shown in typedef above
    // returns the most newly inserted element for which callback function returned true, thus stopping the iteration
    // or returns a null pointer if callback function matched no element (i.e. all retured false) and iteration reaches the end
    // additionally, if extract is set to true it also extracts (i.e. get + removes) the returned node from the list
    T* walk(testNode test, bool extract = false){
      Node *prevNode = NULL;
      Node *curNode = headNode;
      while ( curNode ){
        if ( test(curNode->storedData) ){
          if ( extract ){
            if ( prevNode )
              prevNode->nextNode = curNode->nextNode;
            else
              headNode = curNode->nextNode;
            delete(curNode);
            --nodesCount;
          }
          return &curNode->storedData;
        }
        prevNode = curNode;
        curNode = curNode->nextNode;
      }
      return NULL;
    }

};
